package main

import (
	"net/http"
	"os"

	"github.com/oscarpfernandez/go-tesseract-ocr-service/handlers"
	"github.com/TwiN/g8/v2"
	"github.com/Sirupsen/logrus"
	"github.com/rs/cors"
)

func init() {
	log := logrus.New()
	log.Formatter = new(logrus.TextFormatter) // default
	log.Level = logrus.DebugLevel
}

func main() {
	logrus.Print("Tesseract Rest Service")

	// set up token request filters
	OCRAPITokenString := os.Getenv("OCR_API_TOKEN")
	if len(OCRAPITokenString) == 0 {

		logrus.Panic("OCR_API_TOKEN environment variable not set")

	}

	authorizationService := g8.NewAuthorizationService().WithToken(OCRAPITokenString)
	gate := g8.New().WithAuthorizationService(authorizationService)

	h := handlers.NewHandlers(os.Getenv("UPLOADED_FILES_DIR"))

	router := http.NewServeMux()
	router.HandleFunc("/api/upload/pdf", gate.ProtectFunc(h.UploadPDF))
	router.HandleFunc("/api/upload/img", gate.ProtectFunc(h.UploadImage))
	router.HandleFunc("/web/pdf", gate.ProtectFunc(h.GuiUploadPDF))
	router.HandleFunc("/web/img", gate.ProtectFunc(h.GuiUploadImage))

	// Wrapping the API handler in CORS default behaviors
	c := cors.New(cors.Options{
		AllowedHeaders: []string{"Origin", "Accept", "Content-Type"},
		AllowedMethods: []string{"GET", "POST"},
	})

	// Start server
	err := http.ListenAndServe(":80", c.Handler(router))
	if err != nil {
		logrus.Fatal("Error attempting to ListenAndServe: ", err)
	}
}
